

$('.tabs-title').on('click', (event) => {
    $('.tabs-title').siblings('.active').removeClass('active');
    $('.tabs-content').siblings('.active-cont').removeClass('active-cont');
    $(event.target).addClass('active');
    let dataTabEvent = $(event.target).attr('data-paragraph');

    $('.tabs-content').each((i, elem) => {
        if ($(elem).attr('data-paragraph') === dataTabEvent ) {
            $(event.target).addClass('active');
            $(elem).addClass('active-cont');
        } else {
            $(elem).removeClass('active-cont');
        }
            })
});
